/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packethacker;

import java.io.File;

/**
 *
 * @author daniel
 */
public class PacketMsg {
    public Packet packet;
    public String name;
    public File originalFile;
    private long uid;
    private static long globalObjCount = 0;
    
    public PacketMsg(){
        uid = globalObjCount;
        globalObjCount++;
//        System.out.println("globalObjCount = "+globalObjCount+"\tname = "+name);
    }
    
    public PacketMsg(PacketMsg src){
        packet = new Packet(src.packet);
        name = src.name;
        originalFile = src.originalFile;
        uid = src.uid;
    }
    
    public long getUid(){
        return uid;
    }
}
