/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packethacker;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author daniel
 */
public class DataVisualizerGUI extends javax.swing.JFrame {
    private PacketTableModel tm;
    private ArrayList<PacketMsg> packets;
    private RadixRenderer cellRenderer;
    private boolean showParity;
    
    /**
     * Creates new form DataVisualizerGUI
     */
    public DataVisualizerGUI() {
        initComponents();
        
        this.setTitle("Packet Visualizer");
        
        packets = new ArrayList<>();
        tm = new PacketTableModel();
        
        cellRenderer = new RadixRenderer(8);
        showParity = false;
        showParityCheckBox.setSelected(showParity);
        
        table.setModel(tm);
        table.setDefaultRenderer(Long.class, cellRenderer);
        table.setDefaultRenderer(Packet.EvenOddParityEnum.class, new ParityRenderer());
        
        radixDropdown.removeAllItems();
        radixDropdown.addItem("Dec");
        radixDropdown.addItem("Hex");
        radixDropdown.addItem("Bin");
        
        wordWidthSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                int newWidth = (int)wordWidthSpinner.getValue();
                cellRenderer.setBitWidth(newWidth);
                for(PacketMsg msg: packets){
                    msg.packet.setWordWidth(newWidth);
                    rebuildTableModel();
                }
            }
        });
    }
    
    public void addPacket(PacketMsg msg){
        wordWidthSpinner.setValue(msg.packet.getWordWidth());
        packets.add(msg);
        addPacketToModel(msg);
    }    
    
    public void removePacket(PacketMsg msg){
        for(PacketMsg packetMsg: packets){
            if(packetMsg.getUid() == msg.getUid()){
                System.out.println("Removed "+packetMsg.name+" "+packetMsg.getUid());
                packets.remove(packetMsg);
                break;
            }
        }
        
        rebuildTableModel();
    }
    
    public void setWordWidth(int width){
        wordWidthSpinner.setValue(width);
    }
    
    public void showParity(boolean show){
        showParity = show;
        showParityCheckBox.setSelected(showParity);
    }
    
    private void rebuildTableModel(){
        tm = new PacketTableModel();
        
        for(PacketMsg msg: packets){
            addPacketToModel(msg);
        }
        
        table.setModel(tm);
    }
    
    private void addPacketToModel(PacketMsg msg){
        tm.addColumn(msg.name);
        if(showParity){
            tm.addColumn("Parity");
        }
        
        // clear table of all rows
        tm.setNumRows(0);
        
        for(int r=0; r<packets.get(0).packet.size(); r++){
            Vector row = new Vector();
            for(int p=0; p<packets.size(); p++){
                if(r<packets.get(p).packet.size()){
                    row.add(packets.get(p).packet.getWord(r));
                    if(showParity){
                        row.add(packets.get(p).packet.getParity(r));
                    }
                }
            }
            
            tm.addRow(row);
        }
    }
    
    private static class PacketTableModel extends DefaultTableModel{
        @Override
        public Class<?> getColumnClass(int col) {
            if (getRowCount() == 0) {
                return Object.class;
            } else {
                Object cellValue = getValueAt(0, col);
                return cellValue.getClass();
            }
        }
    }
    
    private static class ParityRenderer extends DefaultTableCellRenderer{
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Color bgColor = Color.LIGHT_GRAY;
            if(value!=null){
                switch((Packet.EvenOddParityEnum)value){
                    case EVEN:
                        value = "Even";
                        bgColor = new Color(103, 147, 218);
                        break;
                    case ODD:
                        value = "Odd";
                        bgColor = new Color(240, 193, 131);
                        break;
                }
            }
            
            Component component = super.getTableCellRendererComponent(
                    table,
                    value,
                    isSelected,
                    hasFocus,
                    row,
                    column);
            
            component.setBackground(bgColor);
            
            return component;
        }
    }
    
    private static class RadixRenderer extends GradientRenderer {
        int bitWidth;
        private RadixEnum radix;
        public enum RadixEnum{
            DEC,HEX,BIN
        };
        
        public RadixRenderer(int bitWidth){
            super();
            this.bitWidth = bitWidth;
            radix = RadixEnum.DEC;
        }
        
        public void setBitWidth(int newWidth){
            bitWidth = newWidth;
        }
        
        public void setRadix(RadixEnum r){
            radix = r;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component component = super.getTableCellRendererComponent(
                    table,
                    value,
                    isSelected,
                    hasFocus,
                    row,
                    column);
            System.out.printf("c=%d v=%d\n",column,value);
            this.setValue(getRadixFormattedString((long)value));
            
            component.setFont(new Font("monospaced", Font.PLAIN, 12));
            
            return component;
        }
        
        private String getRadixFormattedString(long value){
            String strValue = "";
            
            switch(radix){
                case DEC:
                    strValue = Long.toString(value);
                    break;
                case HEX:
                    int nibbleWidth = (int)Math.ceil(bitWidth/4.0);
                    strValue = String.format("0x%"+nibbleWidth+"X", value);
                    strValue = strValue.replace(' ', '0');
                    break;
                case BIN:
                    strValue = Long.toBinaryString(value);
                    strValue = String.format("0b%"+bitWidth+"s", strValue);
                    strValue = strValue.replace(' ', '0');
                    break;
            }// switch
            
            return strValue;
        }
    }
    
    private static class GradientRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {
            Component component = super.getTableCellRendererComponent(
                    table,
                    value,
                    isSelected,
                    hasFocus,
                    row,
                    column);
            
            if(value instanceof Number){
                // get min and max values in table so we can use full color gradient
                long maxVal = Integer.MIN_VALUE;
                long minVal = Integer.MAX_VALUE;
                TableModel tm = table.getModel();
                for(int r=0; r<tm.getRowCount(); r++){
                    for(int c=0; c<tm.getColumnCount(); c++){
                        try{
                            if(table.getValueAt(r,c) instanceof Number){
                                long val = (long)table.getValueAt(r,c);
                                if(val<minVal){
                                    minVal = val;
                                }

                                if(val>maxVal){
                                    maxVal = val;
                                }
                            }
                        } catch(NullPointerException ex){
                            // table has unequal column lengths, this is okay
                        }
                    }
                }

                Color c1 = Color.RED;
                Color c2 = Color.GREEN;
                float steps = 255;
                Color[] gradient = new Color[(int)steps];

                float rSlope = (c2.getRed()-c1.getRed())/steps;
                float gSlope = (c2.getGreen()-c1.getGreen())/steps;
                float bSlope = (c2.getBlue()-c1.getBlue())/steps;

                int startR = c1.getRed();
                int startG = c1.getGreen();
                int startB = c1.getBlue();
                for(int i=1; i<(int)steps+1; i++){
                    int newR = (int)(startR+(rSlope*i));
                    int newG = (int)(startG+(gSlope*i));
                    int newB = (int)(startB+(bSlope*i));

                    gradient[i-1] = new Color(newR,newG,newB);
                }
                
                int mappedVal = (int)map((long)value, minVal, maxVal, 0, 254);
                component.setBackground(gradient[mappedVal]);
            } else {
                component.setBackground(Color.LIGHT_GRAY);
            }
            
            return component;
        }
        
        private long map(
                long val,
                long srcMin,
                long srcMax,
                long dstMin,
                long dstMax) throws IllegalArgumentException{
            long result = 0;

            if(val<srcMin){
                IllegalArgumentException e = new IllegalArgumentException(
                    "val is less than srcMin");
                throw e;
            } else if(val>srcMax) {
                IllegalArgumentException e = new IllegalArgumentException(
                    "val is greater than srcMax");
                throw e;
            } else if(srcMin>srcMax) {
                IllegalArgumentException e = new IllegalArgumentException(
                    "srcMin is greater than srcMax");
                throw e;
            } else if(dstMin>dstMax) {
                IllegalArgumentException e = new IllegalArgumentException(
                    "dstMin is greater than dstMax");
                throw e;
            } else {
                double scale = ((double)(val-srcMin))/(srcMax-srcMin);
                result = (int)(scale*(dstMax-dstMin));
                result += dstMin;
            }

            return result;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        radixDropdown = new javax.swing.JComboBox<>();
        wordWidthSpinner = new javax.swing.JSpinner();
        showParityCheckBox = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(table);

        jToolBar1.setRollover(true);

        radixDropdown.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        radixDropdown.setMinimumSize(new java.awt.Dimension(80, 80));
        radixDropdown.setPreferredSize(new java.awt.Dimension(100, 28));
        radixDropdown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radixDropdownActionPerformed(evt);
            }
        });
        jToolBar1.add(radixDropdown);

        wordWidthSpinner.setMinimumSize(new java.awt.Dimension(50, 28));
        wordWidthSpinner.setPreferredSize(new java.awt.Dimension(50, 28));
        jToolBar1.add(wordWidthSpinner);

        showParityCheckBox.setText("Show Parity");
        showParityCheckBox.setToolTipText("Checking this box will make PacketHacker compute the even/odd bit parity for each packet. It will display the corresponding parity in a new column next to each packet.");
        showParityCheckBox.setFocusable(false);
        showParityCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        showParityCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        showParityCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showParityCheckBoxActionPerformed(evt);
            }
        });
        jToolBar1.add(showParityCheckBox);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void radixDropdownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radixDropdownActionPerformed
        String radix = (String)radixDropdown.getSelectedItem();
        
        if(radix!=null){
            switch(radix){
                case "Dec":
                    cellRenderer.setRadix(RadixRenderer.RadixEnum.DEC);
                    break;
                case "Hex":
                    cellRenderer.setRadix(RadixRenderer.RadixEnum.HEX);
                    break;
                case "Bin":
                    cellRenderer.setRadix(RadixRenderer.RadixEnum.BIN);
                    break;
            }
            rebuildTableModel();
        }
    }//GEN-LAST:event_radixDropdownActionPerformed

    private void showParityCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showParityCheckBoxActionPerformed
        showParity = showParityCheckBox.isSelected();
        rebuildTableModel();
    }//GEN-LAST:event_showParityCheckBoxActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DataVisualizerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DataVisualizerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DataVisualizerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DataVisualizerGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DataVisualizerGUI gui = new DataVisualizerGUI();
                gui.setVisible(true);
                gui.showParity(true);
                
                Packet testPacket = new Packet(8);
                testPacket.addByte((short)0xCA);
                testPacket.addByte((short)0xFE);
                testPacket.addByte((short)0xBA);
                testPacket.addByte((short)0xBE);
                testPacket.addByte((short)0xDE);
                testPacket.addByte((short)0xAD);
                testPacket.addByte((short)0xBE);
                testPacket.addByte((short)0xEF);
                testPacket.addByte((short)0x00);
                
                Packet testPacket2 = new Packet(8);
                testPacket2.addByte((short)0xBB);
                testPacket2.addByte((short)0xAA);
                testPacket2.addByte((short)0xBA);
                testPacket2.addByte((short)0x22);
                testPacket2.addByte((short)0x25);
                testPacket2.addByte((short)0xFE);
                testPacket2.addByte((short)0x11);
                testPacket2.addByte((short)0xEF);
                testPacket2.addByte((short)0x00);
                
                PacketMsg msg1= new PacketMsg(), msg2 = new PacketMsg();
                msg1.packet = testPacket;
                msg1.name = "test";
                msg2.packet = testPacket2;
                msg2.name = "test2";
                gui.addPacket(msg1);
                gui.addPacket(msg2);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JComboBox<String> radixDropdown;
    private javax.swing.JCheckBox showParityCheckBox;
    private javax.swing.JTable table;
    private javax.swing.JSpinner wordWidthSpinner;
    // End of variables declaration//GEN-END:variables
}
