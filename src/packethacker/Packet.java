/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packethacker;

import java.util.ArrayList;

/**
 *
 * @author daniel
 */
public class Packet {
    private ArrayList<Byte> rawData;
    private long totalBitCount;
    
    private int interpretedWordWidth;
    private ArrayList<Long> interpretedWords;
    
    public enum EvenOddParityEnum{
        EVEN,ODD
    }
    
    public Packet(Packet packet){
        rawData = new ArrayList<>(packet.rawData);
        totalBitCount = packet.totalBitCount;
        
        interpretedWordWidth = packet.interpretedWordWidth;
        interpretedWords = new ArrayList<>(packet.interpretedWords);
    }
    
    public Packet(int width){
        rawData = new ArrayList<>();
        interpretedWords = new ArrayList<>();
        
        interpretedWordWidth = width;
        totalBitCount = 0;
    }
    
    public void setWordWidth(int width){
        interpretedWordWidth = width;
        reshape();
    }
    
    public int size(){
        return interpretedWords.size();
    }
    
    public long getWord(int i){
        return interpretedWords.get(i);
    }
    
    public int getWordWidth(){
        return interpretedWordWidth;
    }
    
    public long getBitsInserted(){
        return totalBitCount;
    }
    
    public EvenOddParityEnum getParity(int i){
        boolean isOdd = false;
        long v = getWord(i);
        
        while (v>0){
            isOdd = !isOdd;
            v = v & (v - 1);
        }
        
        return (isOdd?EvenOddParityEnum.ODD:EvenOddParityEnum.EVEN);
    }
    
    public void addByte(short b){
        int bitsUnusedInLastWord = (int)((interpretedWordWidth*interpretedWords.size())-totalBitCount);
        boolean lastWordFilled = bitsUnusedInLastWord<=0;
        
        long bitMask = getBitMask(true, 8, interpretedWordWidth);
        if(lastWordFilled){
            int bitsLeftToInterpret = 8;
            while(bitsLeftToInterpret>0){
                int shiftAmount = bitsLeftToInterpret-interpretedWordWidth;
                boolean isLeftShift = (shiftAmount<=0);
                shiftAmount = Math.abs(shiftAmount);
                
                long interpWord = (b&bitMask);
                if(isLeftShift){
                    interpWord <<= shiftAmount;
                } else {
                    interpWord >>= shiftAmount;
                }// if
                interpretedWords.add(interpWord);
                bitsLeftToInterpret -= interpretedWordWidth;
                bitMask >>= interpretedWordWidth;
            }// while
        } else {
            // fill the previous word first
            int bitsLeftToInterpret = 8;
            while(bitsLeftToInterpret>0){
                int bitsAbleToInsert = (bitsUnusedInLastWord<=bitsLeftToInterpret?bitsUnusedInLastWord:bitsLeftToInterpret);
                long fillMask = getBitMask(true, 8, bitsAbleToInsert);
                fillMask >>= 8-bitsLeftToInterpret;
                long fillData = (b&fillMask)>>(bitsLeftToInterpret-bitsAbleToInsert);
                
                int lastWordIdx = interpretedWords.size()-1;
                long lastWord = interpretedWords.get(lastWordIdx);
                lastWord |= fillData<<(bitsUnusedInLastWord-bitsAbleToInsert);
                interpretedWords.set(lastWordIdx, lastWord);
                bitsUnusedInLastWord -= bitsAbleToInsert;
                bitsLeftToInterpret -= bitsAbleToInsert;
                if(bitsLeftToInterpret>0 && bitsUnusedInLastWord<=0){
                    // we filled this word, yet we still have uninterpreted
                    // bits, so we must add an empty word
                    interpretedWords.add(0L);
                    bitsUnusedInLastWord = interpretedWordWidth;
                }// if
            }// while
        }// if (lastWordFilled)
        
        rawData.add((byte)b);
        totalBitCount += 8;
    }
    
    public String toBinaryString(){
        String result = "";
        String wordStr;
        
        for(int w=0; w<interpretedWords.size(); w++){
            wordStr = Long.toBinaryString(interpretedWords.get(w));
            wordStr = String.format("%"+interpretedWordWidth+"s", wordStr);
            wordStr = wordStr.replace(' ', '0');
            result += wordStr;
        }
        
        return result;
    }
    
    private void reshape(){
        // copy the original data
        ArrayList<Byte> tempData = new ArrayList<>(rawData);
        
        // clear out everything
        rawData.clear();
        totalBitCount = 0;
        interpretedWords.clear();
        
        // add the original data back in, but reinterpreted
        for(int b=0; b<tempData.size(); b++){
            this.addByte((short)(tempData.get(b)&0xFF));
        }
    }
    
    private long getBitMask(boolean msb, int wordSize, int numBits){
        long mask = 0;
        
        if(numBits>wordSize){
            numBits = wordSize;
        }
        
        if(msb){
            for(int i=1; i<=numBits; i++){
                mask |= 1<<wordSize-i;
            }
        } else {
            for(int i=0; i<numBits; i++){
                mask |= 1<<i;
            }
        }
        
        return mask;
    }
    
    public static void main(String[] args) {
        Packet testPacket = new Packet(8);
        testPacket.addByte((short)0xCA);
        testPacket.addByte((short)0xFE);
        testPacket.addByte((short)0xBA);
        testPacket.addByte((short)0xBE);
        testPacket.addByte((short)0xDE);
        testPacket.addByte((short)0xAD);
        testPacket.addByte((short)0xBE);
        testPacket.addByte((short)0xEF);
        
        String expectStr = testPacket.toBinaryString();
        long insertedBits = testPacket.getBitsInserted();
        
        int passCount = 0;
        int testCases = 50;
        for(int width=1; width<testCases+1; width++){
            testPacket.setWordWidth(width);
            String actualStr = testPacket.toBinaryString();
            
            actualStr = actualStr.substring(0, (int)insertedBits);
            
            if(expectStr.compareTo(actualStr)==0){
                passCount++;
            } else {
                System.out.println("Failed to set words width to "+width);
                System.out.printf("expect: %s\n", expectStr);
                System.out.printf("actual: %s\n", actualStr);
            }
        }
        
        if(passCount!=testCases){
            System.out.printf("FAILED: reShapeTest (%d passed out of %d)\n",passCount,testCases);
        } else {
            System.out.printf("OK: reShapeTest (%d passed)\n",passCount);
        }
    }
}
